import React, { Component } from 'react';
import { Image, TouchableOpacity, Animated, View,  } from 'react-native';
import { Card, Content } from 'native-base';
// import Icon from 'react-native-vector-icons/FontAwesome';
// import styles from "./styles";

class ImageList extends Component {
  constructor(props) {
    super(props);
    this.data = props.data;
  }
  _handlePress = () => {
    const { imageLink, } = this.props;  
    const imageCaption = this.data.title;
    this.props.onPress({ imageLink, imageCaption });
    // console.log(imageLink, imageCaption)
  };


  render() {
    const { itemWidth, imageLink,  } = this.props;
    // console.log(image);

    return (
      <Content  contentContainerStyle={{ alignItems: 'center'}}>
      <Card >
        <TouchableOpacity
          onPress={this._handlePress}
          activeOpacity={0.5}
        >
          <Image
            style={{ width: itemWidth, height: 100 }}
            source={{ uri: imageLink }}
          />
          
        </TouchableOpacity>
        
      </Card>
      </Content>
    );
  }
}

export default ImageList;
