import React, { Component } from 'react';
import { View, Text, Image, } from 'react-native';
import MyLogo from './Logo/MyLogo';
import { Container, Button, Spinner } from './widgets';

// const footerBanner = require('../../assets/img/icons/bespokenBanner.png');
const footerLogo = require('../../assets/img/icons/myLogoHorse.png');
class Login extends Component {

    constructor(props){
        super(props);       
    }


    onLoginPress = () => {
        const { navigation } = this.props;
        navigation.navigate('Home');
    
        
    }
  

    renderButton=(buttonText, buttonColor, textColor ) => {
        if(this.props.loading) {
            return <Spinner size='large' />
        }
        return (
            <Button 
                onPress={this.onLoginPress}
                // onPress={() => this.onLoginPress (buttonText)}
                modButtonStyle= {{ backgroundColor: buttonColor }} 
                modTextStyle= {{ color: textColor }} 
            >
                {buttonText}

            </Button>
        )
    }
  
    render() {
        
        return(
            <Container  >
                <MyLogo />
                <View style={{ height: 20 }} />
                
                <View style={{ 
                    paddingHorizontal: 10,
                    paddingVertical: 5,
                    height: 70,
                    width: '90%',
                    alignItems: 'center',
                    justifyContent: 'center'
                    }}>
                    {/* {this.renderButton('Sign In', 'yellow', 'black')}
                    { this.renderButton('Sign Up','cyan', '#000') } */}
                    {this.renderButton('Start', '#fff', '#006600')}
                </View>
                <View style={styles.footer} > 
                    <Text style={styles.copyrightStyle}> © </Text>
                    <Text style={{ fontSize: 15, color: '#006600', justifyContent: 'center', alignItems: 'center', marginLeft: 10 }}> 2018 </Text>
                    <Text style={styles.textStyles}>   beSpokenIT Services </Text>
                    {/* <Image 
                        source={footerBanner}
                        style={styles.bespokenBanner}>
                    </Image> */}
                    
                    <Image 
                        source={footerLogo}
                        style={styles.footerLogo}>
                    </Image>
                    
                    
                </View>
        </Container>
        );
    }
}

export default Login;

const styles = {
    footer:{
        position: 'absolute',
        bottom: 0,
        flexDirection: 'row',
        // left: 0,
        width: '100%',
        height: 70,
        backgroundColor: '#fff',
        alignItems: 'center',
        justifyContent: 'space-around',
        // paddingHorizontal: 10,

    },
    bespokenBanner:{
        resizeMode: 'contain',
        // width: 425,
        height: 40,

    },
    footerLogo:{
        resizeMode: 'contain',
        marginLeft: 11,
        // marginRight: 5,
        height: 22,
        // paddingHorizontal: 10,
        alignItems: 'center',
        justifyContent: 'center',

    },
    textStyles: {
        fontSize: 18,
        // alignSelf: 'center',
        marginLeft: 25,
        alignItems: 'center',
        justifyContent: 'center',
        color: '#000'
    },
    copyrightStyle: {
        fontSize: 24,
        color: '#006600',
        // paddingHorizontal: 5,
        alignItems: 'center',
        justifyContent: 'center',
        marginRight: 10,
        marginLeft: 20,
    }
}