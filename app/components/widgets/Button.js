import React, { PureComponent } from 'react';
import { Text, TouchableOpacity, View , StyleSheet, } from 'react-native';
import { Ionicons } from '@expo/vector-icons';

// Custom button that recieves buttonPress, Button Title, text style and button style

export  class Button extends PureComponent {
// use static default statement to define initial props
  static defaultProps = {
    backgroundColor: "black",
    color: "white",
    title: " ",
    radius: 35,
    style: { }
  }
  // constructor(props){
  //   super(props);
  //   this.state = {
  //     backgroundColor: "black",
  //     color: "white",
  //     title: "",
  //     radius: 40,
  //   }
  // }

  render (buttonText, buttonColor, textColor) {
    const r = this.props.radius
    const w = this.props.radius * 2 //allows button to be a circular
    const h = this.props.radius * 2 //allows button to be a circular
    const bgC = this.props.backgroundColor

    const { container, textStyle, iconLabelStyle, icon } = styles;

    return (
      <View>
        <TouchableOpacity 
          onPress={this.props.onPress} 
          style={[ container, icon, {...this.props.style } ]}>
          <Ionicons
            name={this.props.iconName}
            // style={{
            //   width: 40,
            //   height: 40,
            //   justifyContent: "center",
            //   color: "#00ff00",
            //   fontSize: 30
            // }}
            // onPress={() => navigate('News')}
        />
          
        </TouchableOpacity>
        <View style={iconLabelStyle}>
          <Text style={[textStyle, { color: this.props.color }, {...this.props.style } ]}>
            {this.props.title}
          </Text>
        </View>
      </View>
    )

  }
}

const styles = StyleSheet.create({
  textStyle: {
    fontSize: 10,
    fontWeight: 'bold',
    
  },
  iconLabelStyle: {
    alignItems: 'center',
    justifyContent: 'center',
  },
  icon: {
    width: 40,
    height: 40,
    justifyContent: 'center',
    color: '#fff',
    fontSize: 30,
  },
  container: {
    alignItems: 'center',
    justifyContent: 'center',
    margin: 10,
    // padding: 2,
  }
})