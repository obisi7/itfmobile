// Import libraries for making a component
import React, { Component} from 'react';
import { Text, View, Image, } from 'react-native';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';


// Make a component
export class Header extends Component {

  static defaultProps = {
    backgroundColor: "black",
      color: "yellow",
      headerText: "My Own Machine",
  }
  
  constructor(props){
    super(props);
    this.state = {
      headerText: ""
    }
  }

  render(){

    const { textStyle, viewStyle } = styles;

    return (
      <View style= { [ viewStyle, 
        { backgroundColor: this.props.backgroundColor, 
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center',
        } 
      ] } >
      <View style={{ justifyContent: 'center', flexDirection: 'row', alignItems: 'center', marginLeft: 10 }} >
        <Image source={this.props.imageLeft} 
          style={this.props.imageHeaderLeft} 
        />
        <Icon name={this.props.iconLeftName} style={this.props.iconLeftStyle} onPress={this.props.leftIconPress} />
        <Icon name={this.props.iconFb} style={this.props.iconFbStyle} onPress={this.props.fbIconPress} />
        <Icon name={this.props.iconTwitter} style={this.props.iconTwitterStyle} onPress={this.props.twitterIconPress} />
        <Icon name={this.props.iconYoutube} style={this.props.iconYoutubeStyle} onPress={this.props.youTubeIconPress} />
      </View>

      <View style={{ justifyContent: 'center', alignItems: 'center'}} >
        <Text style={ [ textStyle, {color: this.props.color } ] }>{this.props.headerText}</Text>
      </View>
      <View style={{ justifyContent: 'center', flexDirection: 'row', alignItems: 'center'}} >
        <Icon name={this.props.iconRightName} 
          style={this.props.iconRightStyle} 
          onPress={this.props.rightIconPress} 
        />
        <Image source={this.props.imageRight} 
          style={this.props.imageHeaderRight} 
        />
      </View>
    </View>
    ) 
  }
}

const styles = {
  viewStyle: {
    width: null,
    // backgroundColor: '#F8F8F8',
    borderWidth: 0,
    justifyContent: 'center',
    alignItems: 'center',
    height: 65,
    // paddingTop: 0,
    // shadowColor: '#000',
    // shadowOffset: { width: 0, height: 2 },
    // shadowOpacity: 0.2,
    // elevation: 2,
    // position: 'relative'
  },
  textStyle: {
    fontSize: 22,
    color: '#fff',
  }
};

// Make the component available to other parts of the app
// export default Header ;