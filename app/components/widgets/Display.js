import React, { PureComponent } from 'react';
import { View, Text, StyleSheet, TouchableOpacity, Linking, Dimensions } from 'react-native';
// import { createStackNavigator, createDrawerNavigator, createAppContainer } from 'react-navigation';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';
import {
  Grid,
  Col,
  Row,
} from 'native-base';
const deviceHeight = Dimensions.get("window").height;

export class Display extends PureComponent {

  constructor(props){
    super(props);
    this.state = {
      display: ""
    }
  }

  render() {
    // const { navigate } = this.props.navigation;
    return (
      <View style={[ styles.container, { backgroundColor: this.props.backgroundColor } ]}>

            <Grid>
              <Row style={styles.row}>
                <Col style={styles.col}>
                  <TouchableOpacity>
                    <Icon
                      name="calendar-clock"
                      style={styles.icon}
                      onPress={() => this.props.navigation.navigate('Events')}
                    />
                  </TouchableOpacity>
                  <Text numberOfLines={1} style={styles.iconText}>
                    Events
                  </Text>
                </Col>

                <Col style={styles.col}>
                  <TouchableOpacity>
                    <Icon
                      name="rss"
                      style={styles.icon}
                      onPress={() => this.props.navigation.navigate('News')}
                    />
                    <Text numberOfLines={1} style={styles.iconText}>
                      News
                    </Text>
                  </TouchableOpacity>
                </Col>
                <Col style={styles.col}>
                  <Icon
                    name="image-multiple"
                    style={styles.icon}
                    onPress={() => this.props.navigation.navigate('Photos')}
                  />
                  <Text numberOfLines={1} style={styles.iconText}>
                    Photos
                  </Text>
                </Col>
                <Col style={styles.col}>
                  <TouchableOpacity>
                    <Icon
                      name="video"
                      style={styles.icon}
                      onPress={() => this.props.navigation.navigate('Videos')}
                    />
                    <Text numberOfLines={1} style={styles.iconText}>
                      Videos
                    </Text>
                  </TouchableOpacity>
                </Col>
              </Row>

              <Row style={styles.row}>
                <Col style={styles.col}>
                  <TouchableOpacity>
                    <Icon
                      name="office"
                      style={styles.icon}
                      onPress={() => Linking.openURL('http://my.morgan.edu')}
                    />
                  </TouchableOpacity>
                  <Text numberOfLines={1} style={styles.iconText}>
                    myMSU
                  </Text>
                </Col>
                <Col style={styles.col}>
                  <TouchableOpacity>
                    <Icon
                      name="weather-rainy"
                      style={styles.icon}
                      onPress={() => this.props.navigation.navigate('Weather')}
                    />
                    <Text numberOfLines={1} style={styles.iconText}>
                      Weather
                    </Text>
                  </TouchableOpacity>
                </Col>
                <Col style={styles.col}>
                  <TouchableOpacity>
                    <Icon
                      name="radio"
                      style={styles.icon}
                      onPress={() =>
                        Linking.openURL('http://amber.streamguys.com:4020/live')
                      }
                    />
                    <Text numberOfLines={1} style={styles.iconText}>
                      WEAA
                    </Text>
                  </TouchableOpacity>
                </Col>

                <Col style={styles.col}>
                  <Icon
                    active
                    name="bus-school"
                    style={styles.icon}
                    onPress={() =>
                      // this.handleOpenURL("transloc://")
                      Linking.openURL('http://morgan.transloc.com')
                    }
                  />
                  <Text numberOfLines={1} style={styles.iconText}>
                    {' '}
                    Shuttle{' '}
                  </Text>
                </Col>
              </Row>

              <Row style={styles.row}>
                <Col style={styles.col}>
                  <Icon
                    active
                    name="football"
                    style={styles.icon}
                    onPress={() => this.props.navigation.navigate('Athletics')}
                  />
                  <Text numberOfLines={1} style={styles.iconText}>
                    Athletics
                  </Text>
                </Col>

                <Col style={styles.col}>
                  <Icon
                    name="school"
                    style={styles.icon}
                    onPress={() =>
                      Linking.openURL('http://www.morgan.edu/alumni_and_friends.html')
                    }
                  />
                  <Text numberOfLines={1} style={styles.iconText}>
                    Alumni
                  </Text>
                </Col>
                <Col style={styles.col}>
                  <Icon
                    name="credit-card-multiple"
                    style={styles.icon}
                    onPress={() => Linking.openURL('http://givetomorgan.org')}
                  />
                  <Text numberOfLines={1} style={styles.iconText}>
                    Give
                  </Text>
                </Col>
                <Col style={styles.col}>
                  <Icon
                    active
                    name="map-marker"
                    style={styles.icon}
                    onPress={() => Linking.openURL('http://map.morgan.edu')}
                  />
                  <Text numberOfLines={1} style={styles.iconText}>
                    Map
                  </Text>
                </Col>
              </Row>

              <Row style={styles.row}>
                <Col style={styles.col}>
                  <Icon
                    name="cart"
                    style={styles.icon}
                    onPress={() =>
                      Linking.openURL('http://morgan.bncollege.com/webapp/wcs/stores/servlet/BNCBHomePage?storeId=88866&catalogId=10001&langId=-1')
                    }
                  />
                  <Text numberOfLines={1} style={styles.iconText}>
                    Bookstore
                  </Text>
                </Col>
                <Col style={styles.col}>
                  <Icon
                    active
                    name="food"
                    style={styles.icon}

                    onPress={() =>
                      Linking.openURL('https://www.dineoncampus.com/morgan/')
                    }
                  />
                  <Text numberOfLines={1} style={styles.iconText}>
                    Dining
                  </Text>
                </Col>
                <Col style={styles.col}>
                  <Icon
                    name="human-male-female"
                    style={styles.icon}
                    onPress={() =>
                      Linking.openURL('https://whse2.morgan.edu/apex/f?p=372:1')
                    }
                  />
                  <Text numberOfLines={1} style={styles.iconText}>
                    Find
                  </Text>
                </Col>
                <Col style={styles.col}>
                  <Icon
                    active
                    name="near-me"
                    style={styles.icon}
                    onPress={() => this.props.navigation.navigate('NearMe')}
                  />
                  <Text numberOfLines={1} style={styles.iconText}>
                    Near Me
                  </Text>
                </Col>
              </Row>
              <Row style={styles.row}>
                <Col style={styles.col}>
                  <Icon
                    name="alert"
                    style={styles.icon}
                    onPress={() =>
                      Linking.openURL('http://www.morgan.edu/student_affairs/police_and_public_safety/mobile_alert_system/account_login.html')
                    }
                  />
                  <Text numberOfLines={1} style={styles.iconText}>
                    Alerts
                  </Text>
                </Col>
                <Col style={styles.col}>
                  <Icon
                    active
                    name="phone-outgoing"
                    style={styles.icon}

                    onPress={() =>
                      Linking.openURL('http://www.morgan.edu/contactus/')
                    }
                  />
                  <Text numberOfLines={1} style={styles.iconText}>
                    Contacts
                  </Text>
                </Col>
                <Col style={styles.col}>
                  <Icon
                    name="library-books"
                    style={styles.icon}
                    // style={{
                    //   width: 40,
                    //   height: 40,
                    //   justifyContent: "center",
                    //   color: "#00ffff",
                    //   fontSize: 30
                    // }}
                    onPress={() =>
                      Linking.openURL('http://www.morgan.edu/library')
                    }
                  />
                  <Text numberOfLines={1} style={styles.iconText}>
                    Library
                  </Text>
                </Col>
                <Col style={styles.col}>
                  <Icon
                    active
                    name="lead-pencil"
                    style={styles.icon}
                    onPress={() =>
                      Linking.openURL('http://www.morgan.edu/apply_now.html')
                    }
                  />
                  <Text numberOfLines={1} style={styles.iconText}>
                    Apply
                  </Text>
                </Col>
              </Row>
            </Grid>
          
      </View>
    )
  }
}


const styles = StyleSheet.create({

  container: {
    flex: 1,
    width: null,
    backgroundColor: 'transparent',
  },
  horizontalLine: {
    borderBottomColor: '#f47937',
    borderBottomWidth: 1,
  },
  iconContainer: {
    flexDirection: 'row',
    flexWrap: 'wrap',
    justifyContent: 'space-around',
    paddingLeft: 15,
  },
  iconText: {
    fontSize: 16,
    color: '#fff',
    marginTop: 2,
  },
  icon: {
    width: 40,
    height: 40,
    justifyContent: 'center',
    color: '#fff',
    fontSize: 30,
  },
  col: {
    alignItems: 'center',
    paddingHorizontal: 3,
  },
  row: {
    paddingBottom: 30,
  },
  contentImageStyle: {
    alignSelf: "stretch",
    // resizeMode: 'cover',
    height: deviceHeight / 3.5,
    width: null,
    position: "relative",
    marginBottom: 10
  }
})
