import React, { Component } from "react";
import { View, FlatList, ActivityIndicator, Alert } from "react-native";
import {
  Container,
  Content,
  Text,
  ListItem
} from "native-base";
// import Icon from "react-native-vector-icons/FontAwesome";
import styles from "./styles";
import ModalNewsView from "./modalNewsView";
import NewsData from "../newsdata/";
import xmlToJson from '../../utils/xmlToJson';
// import xml2js from 'react-native=xml2js';

export class News extends Component {
  constructor(props) {
    super(props);
    this.state = {
      data: [],
      isLoading: true,
      refreshing: false,
      text: "",
      setModalVisible: false,
      modalArticleData: {},
      error: null
    };
  }

  // xml to Json converter function

// xmlToJson = (xml) => {
//   // Create the return object
//   let obj = {};

//   if (xml.nodeType === 1) { // element
//     // do attributes
//     if (xml.attributes.length > 0) {
//       obj['@attributes'] = {};
//       for (let j = 0; j < xml.attributes.length; j += 1) {
//         const attribute = xml.attributes.item(j);
//         obj['@attributes'][attribute.nodeName] = attribute.nodeValue;
//       }
//     }
//   } else if (xml.nodeType === 3) { // text
//     obj = xml.nodeValue;
//   }

//   // do children
//   // If just one text node inside
//   if (xml.hasChildNodes() && xml.childNodes.length === 1 && xml.childNodes[0].nodeType === 3) {
//     obj = xml.childNodes[0].nodeValue;
//   } else if (xml.hasChildNodes()) {
//     for (let i = 0; i < xml.childNodes.length; i += 1) {
//       const item = xml.childNodes.item(i);
//       const nodeName = item.nodeName;
//       if (typeof (obj[nodeName]) === 'undefined') {
//         obj[nodeName] = xmlToJson(item);
//       } else {
//         if (typeof (obj[nodeName].push) === 'undefined') {
//           const old = obj[nodeName];
//           obj[nodeName] = [];
//           obj[nodeName].push(old);
//         }
//         obj[nodeName].push(xmlToJson(item));
//       }
//     }
//   }
//   return obj;
// }

  _handleItemDataOnPress = articleData => {
    this.setState({
      setModalVisible: true,
      modalArticleData: articleData
    });
  };

  _handleModalClose = () => {
    this.setState({
      setModalVisible: false,
      modalArticleData: {}
    });
  };

  _handleSearch = e => {
    let text = e.toLowerCase();
    let fullList = this.state.data;
    let filteredList = fullList.filter(item => {
      // search from a full list, and not from a previous search results list
      if (
        item.title
          .toString()
          .toLowerCase()
          .match(text)
      )
        return item;
    });
    if (!text || text === "") {
      this.setState({
        data: fullList,
        noData: false
      });
    } else if (!filteredList.length) {
      // set no data flag to true so as to render flatlist conditionally
      this.setState({
        noData: true
      });
    } else if (Array.isArray(filteredList)) {
      this.setState({
        noData: false,
        data: filteredList
      });
    }
  };

  getNews() {
    // const link = "http://feeds.news24.com/articles/nigeria/national/rss"; // Nigeria News24 channel
    // const link = "http://saharareporters.com/feeds/latest/feed"; // Sahara Reporters Latest News Feed
    // const link = "https://thenationonlineng.net/feed"; // The Nations News Feed
    // const link = "https://www.vanguardngr.com/feed/"; // The Vangaurd News Feed
    // const link = "https://rss.thenigerianvoice.com/news.xml?cat_id=1&group_id=5"; // Nigerian Voice news channel
    const link = "https://feeds.feedburner.com/morganstateu"; // Morgan State news channel
    // const link = "http://rss.cnn.com/rss/cnn_topstories.rss"; // CNN Top Stories
    // const parseString = require("xml2js").parseString; // convert fetched xml data into a json object (old tool)
    // let resJson = [];

    return fetch(link)
      .then(response => response.text())
      .then(response => {
        // parseString(response, function(err, result) {
        //   resJson = result; // local var result saved to a global var resJson for later use
        // });\
        // DOMParser only applies to web browsers, so we need an intermediate step
        // to create a var that works with node.js before calling xmlToJson function
        DOMParser = require('xmldom').DOMParser;
        let result = new DOMParser().parseFromString(response, 'text/xml');
        let resJson = xmlToJson(result);
        // console.log(resJson);
        this.setState({
          data: resJson.rss.channel.item,
          isLoading: false,
          refreshing: false,
          error: response.error || null
        });
      })
      .catch(error => {
        this.setState({ error, isLoading: false });
        // Alert.alert("Error", "Sorry, something went wrong. Please try again");
        // console.log('fetch', error)
      });
  }

  componentDidMount() {
    this.getNews();
  }

  render() {
    // console.log(this.state);
    let view = this.state.isLoading ? (
      <View style={{ flex: 1, alignItems: "center", justifyContent: "center" }}>
        <Text
          style={{ marginTop: 10 }}
          children="Please wait... Loading news items"
        />
        <ActivityIndicator
          animating={this.state.isLoading}
          color="#f47937"
          fontSize="30"
        />
      </View>
    ) : (
      <FlatList
        data={this.state.data}
        extraData={this.state}
        renderItem={({ item, }) => (
          <ListItem>
            <NewsData onPress={this._handleItemDataOnPress} data={item} />
          </ListItem>
        )}
        keyExtractor = {(item, index) => {
          return `${item.title + index}`
          }}
        // keyExtractor={item => item.title}
        // refreshing={this.state.refreshing}
        // onRefresh={this.handleRefresh}
      />
    );

    return (
      <Container style={styles.container}>
        <Content>
          {/* <Header
            searchBar
            rounded
            style={styles.headerTitleStyle}
            iosBarStyle="light-content"
          >
            <Left>
              <Button
                transparent
                onPress={() => this.props.navigation.goBack()}
              >
                <Icon name="angle-left" style={styles.headerIconStyle} />
              </Button>
            </Left>
            <Body>
              <Item>
                <Icon name="search" style={{ color: "#fff", fontSize: 25 }} />
                <Input
                  placeholder="Type Here..."
                  onChangeText={text => this._handleSearch(text)}
                  value={this.state.text}
                  underlineColorAndroid="transparent"
                />
              </Item>
            </Body>

            <Right>
              <Button transparent>
                <Text>Search</Text>
              </Button>
            </Right>
          </Header> */}
          <View style={styles.horizontalLine} />

          {view}
          <ModalNewsView
            showModal={this.state.setModalVisible}
            articleData={this.state.modalArticleData}
            onClose={this._handleModalClose}
          />
        </Content>
      </Container>
    );
  }
}

// export default News;
