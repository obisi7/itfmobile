import { StyleSheet } from 'react-native';

export default  StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: "#ffffff",
    },
  text: {
    alignSelf: "center",
    marginBottom: 7,
      fontSize: 10,
  },
    headerTitleStyle: {
        flex: 4,
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'center',
        backgroundColor: '#f47937',
    },
    headerStyle: {
        flex: 1,
        backgroundColor: '#f47937',

    },
    horizontalLine:{
        borderBottomColor: '#1b4383',
        borderBottomWidth: 1
    },

    myListStyle: {
        flex: 6,
    },
    headerIconStyle: {
        color: '#fff',
        fontSize: 30
    },
})
