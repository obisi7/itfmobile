import React, { Component } from "react";
import {
  Dimensions,
  AppState,
  WebView,
  Modal,
  Share,
  TouchableOpacity,
} from "react-native";
// import { Video } from 'expo';
import {
  Container,
  Header,
  Title,
  Content,
  Left,
  Body,
  Right,
} from "native-base";
import Icon from "react-native-vector-icons/MaterialIcons";
const webViewHeight = Dimensions.get("window").height - 56;

export default class ModalVideoViewer extends Component {
  constructor(props) {
    super(props);
    this.state = {
      appState: AppState.currentState,
      isReady: false,
    };
  }

  _handleClose = () => {
    return this.props.onClose();
  };

  _handleShare = (clickedLink, clickedTitle) => {
    // const { videoId,title } = this.props.data;
    message = `${clickedTitle}\n\nRead more @\n${clickedLink}\n\nshared via MSU mobile`;
    return Share.share(
      { clickedTitle, message, url: message },
      { dialogTitle: `Share ${clickedTitle}` }
    );
  };
    componentDidMount() {
        AppState.addEventListener('change', this._handleAppStateChange);
    }
  
    componentWillUnmount() {
        AppState.removeEventListener('change', this._handleAppStateChange);
    }
  
    _handleAppStateChange = (nextAppState) => {        
        this.setState({appState: nextAppState});
    }
  

  render() {
    const YOUTUBE_API = "AIzaSyCq3IysjyfDAU_QYursgiYjnxwKp0wPy14";
    const { showModal, articleData } = this.props;
    // console.log(articleData.youtubeId);
    const { youtubeId, videoTitle } = articleData;

    return (
      <Modal
        onRequestClose={this._handleClose}
        visible={showModal}
        transparent
        animationType="slide"
      >
        <Container
          // style={{ margin: 0, marginBottom: 0, backgroundColor: "#ffffff" }}
        >
          <Content contentContainerStyle={{ height: webViewHeight }}>
            <Header
              style={{ backgroundColor: "#1b4383" }}
              iosBarStyle="light-content"
            >
              <Left>
                <TouchableOpacity onPress={() => this._handleClose()}>
                  <Icon
                    name="close"
                    style={{ fontSize: 30, color: "#f47937" }}
                  />
                </TouchableOpacity>
              </Left>
              <Body style={{ flex: 5, }}>
                <Title
                  children='Our Youtube Channel'
                  // children={youtubeId}
                  style={{ color: "#ffffff", fontSize: 20 }}
                />
              </Body>
              <Right>
                <TouchableOpacity
                  onPress={() =>
                    this._handleShare(
                      youtubeId,
                      videoTitle
                    )
                  }
                >
                  <Icon
                    name="share"
                    style={{ fontSize: 30, color: "#f47937" }}
                  />
                </TouchableOpacity>
              </Right>
            </Header>
            {/* <Video
              source={{ uri: `https://youtu.be/${youtubeId}` } }
              // source={{ uri: 'http://d23dyxeqlo5psv.cloudfront.net/big_buck_bunny.mp4' } }
              rate={1.0}
              volume={1.0}
              isMuted={false}
              resizeMode="cover"
              shouldPlay
              isLooping
              style={{ width: 300, height: 300 }}
            /> */}
            {/*WebView is the only way to play youtube in expo for now
              You need to listen for the appState event to ensure a video playing,
              stops playing when its screen is changed. See below
            */}
            {this.state.appState == 'active' &&
              <WebView 
                  style={{flex:1}}
                  javaScriptEnabled={true}
                  source={{uri: `https://www.youtube.com/embed/${youtubeId}?rel=0&autoplay=0&showinfo=0&controls=0`}}
              />
            }
          </Content>
        </Container>
      </Modal>
    );
  }
}
const styles = {
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: "#f5fcff"
  },
  welcome: {
    fontSize: 20,
    textAlign: "center",
    margin: 10
  },
};
