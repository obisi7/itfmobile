import React, { Component } from 'react';
import { TouchableOpacity, Image, } from 'react-native';
import {
    Button, Thumbnail, Text,
     Body, Right,
} from "native-base";
import Icon from 'react-native-vector-icons/FontAwesome';

import TimeAgo from 'react-native-timeago';
import  { GetImage } from "../helper/";
import styles from "./styles";

const imagePlaceholder = require('../../../assets/images/itfLogo.png')

class NewsData extends Component{

    constructor(props){
        super(props);
        this.data = props.data;
    }

    _handlePress=() =>{
        const {link, title} = this.data;
        this.props.onPress({link,title})
    };

    render(){
        
        return(
            <TouchableOpacity onPress={this._handlePress} style={{flexDirection:'row'}} activeOpacity={0.5}>
                {/* <Image
                    source={{ cache:'force-cache', uri: "http://image-api.suckup.de/image-api.php?file="+ GetImage(this.data.description["#cdata-section"]) +"&quality=25" }}
                    // source={{ cache:'force-cache', uri: "http://image-api.suckup.de/image-api.php?file="+ GetImage(this.data.description) +"&quality=25" }}
                    style={{ width: 80, height: 45 }}
                /> */}
                {/* <Thumbnail style={{ alignSelf: 'center' }} source={{ cache:'force-cache', uri: "http://image-api.suckup.de/image-api.php?file="+ GetImage(this.data.description) +"&quality=25" }}/>  */}
                <Thumbnail style={{ alignSelf: 'center' }} source={{ cache:'force-cache', uri: "http://image-api.suckup.de/image-api.php?file="+ GetImage(this.data.description["#cdata-section"]) +"&quality=25" }}/> 
                <Body style={styles.myListStyle}>
                    <Text style={{fontSize: 15}}> {this.data.title} </Text>
                    {/* <Text style={{fontSize: 15}}> {this.data.title["#cdata-section"]} </Text> */}
                    <Text note>Posted: {<TimeAgo time={this.data.pubDate.toString()}/>} </Text>
                </Body>
                <Right>
                    <Button transparent>
                        <Icon  name='angle-right' style={{fontSize: 30}}/>

                    </Button>
                </Right>
            </TouchableOpacity>
        )
    }
}
export default NewsData;
