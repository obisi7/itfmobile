import React, { Component } from "react";
import {
  Dimensions,
  AppState,
  WebView,
  Modal,
  Share,
  TouchableOpacity,
  View,
} from "react-native";
// import { Video } from 'expo';
import {
  Container,
  Header,
  Title,
  Content,
  Left,
  Body,
  Right,
} from "native-base";
import Icon from "react-native-vector-icons/MaterialIcons";
const webViewHeight = Dimensions.get("window").height - 56;

export default class ModalPopup extends Component {
  constructor(props) {
    super(props);
    this.state = {
      appState: AppState.currentState,
      isReady: false,
    };
  }

  _handleClose = () => {
    return this.props.onClose();
  };

  _handleShare = (clickedLink, clickedTitle) => {
    // const { videoId,title } = this.props.data;
    message = `${clickedTitle}\n\nRead more @\n${clickedLink}\n\nshared via MSU mobile`;
    return Share.share(
      { clickedTitle, message, url: message },
      { dialogTitle: `Share ${clickedTitle}` }
    );
  };
    componentDidMount() {
        AppState.addEventListener('change', this._handleAppStateChange);
    }
  
    componentWillUnmount() {
        AppState.removeEventListener('change', this._handleAppStateChange);
    }
  
    _handleAppStateChange = (nextAppState) => {        
        this.setState({appState: nextAppState});
    }
  

  render() {
    const { showModal, url, title, headerBgColor } = this.props;

    return (
      <Modal
        onRequestClose={this._handleClose}
        visible={showModal}
        transparent
        animationType="slide"
      >
        <Container>
          <Content contentContainerStyle={{ height: webViewHeight }}>
            <Header
              style={{ backgroundColor: headerBgColor }}
              iosBarStyle="light-content"
            >
              <Left>
                <TouchableOpacity onPress={() => this._handleClose()}>
                  <Icon
                    name="close"
                    style={{ fontSize: 30, color: "#fff" }}
                  />
                </TouchableOpacity>
              </Left>
              <Body style={{ flex: 5, }}>
                {/* <Text>{ modalTitle }</Text>
                  style={{ color: "#fff", fontSize: 20 }}
                /> */}
                <Title
                  children={title}
                  // children={youtubeId}
                  style={{ color: "#fff", fontSize: 20 }}
                />
              </Body>
              {/* <Right>
                <TouchableOpacity
                  onPress={() =>
                    this._handleShare(
                    //   youtubeId,
                    //   videoTitle
                    )
                  }
                >
                  <Icon
                    name="share"
                    style={{ fontSize: 30, color: "#f47937" }}
                  />
                </TouchableOpacity>
              </Right> */}
              <Right />
            </Header>
            <View style={styles.horizontalLine} />
            {/* <Video
              source={{ uri: `https://youtu.be/${youtubeId}` } }
              // source={{ uri: 'http://d23dyxeqlo5psv.cloudfront.net/big_buck_bunny.mp4' } }
              rate={1.0}
              volume={1.0}
              isMuted={false}
              resizeMode="cover"
              shouldPlay
              isLooping
              style={{ width: 300, height: 300 }}
            /> */}
            {/*WebView is the only way to play youtube in expo for now
              You need to listen for the appState event to ensure a video playing,
              stops playing when its screen is changed. See below
            */}
            {this.state.appState == 'active' &&
              <WebView 
                  style={{flex:1}}
                  javaScriptEnabled={true}
                  source={{uri: url}}
                //   source={{uri: 'http://amber.streamguys.com:4020/live'}}
              />
            }
          </Content>
        </Container>
      </Modal>
    );
  }
}
const styles = {
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: "#f5fcff"
  },
  horizontalLine: {
    borderBottomColor: '#000',
    borderBottomWidth: 1,
  },
  welcome: {
    fontSize: 20,
    textAlign: "center",
    margin: 10
  },
};
