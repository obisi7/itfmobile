import React, { Component } from "react";
import {
  View,
  ImageBackground
} from "react-native";
// import Icon from "react-native-vector-icons/Ionicons";
import {
  Container,
  Content,
} from "native-base";
import { Today } from "./todayView";
import styles from "./styles";
// import { getWeather } from '../../weatherAPI';

// const IconNames = {
//   clear: "ios-sunny",
//   rain: "md-rainy",
//   thunderstorm: "md-thunderstorm",
//   cloudy: "md-cloudy",
//   snow: "md-snow",
//   fog: "md-umbrella"
// };
// const ITEM_WIDTH = Dimensions.get("window").width;

export class MyToday extends Component {
  constructor(props) {
    super(props);
    this.state = {
      curLocation: [],
      weatherData: [],
      weatherDataCurrent: [],
    };
  }
  // componentWillMount() {
  //   // Location.requestWhenInUseAuthorization();
  //   // Location.requestAlwaysAuthorization()
  //   // Location.startUpdatingLocation()
  //   // Location.setDistanceFilter(5.0)
  //   // DeviceEventEmitter.addListener('locationUpdated', (location) => {
  //   //   this.setState({'location':location})
  //   // })
  // }

  componentDidMount() {
    this.getLocation(); // gets current location lat and lon

    //MSU: 39.344,-76.58 My apartment: 39.4,-76.6 geolocations
    // this._getWeather(39.344, -76.58); //.then(res => console.log(res));
    // this._getForecast(39.344, -76.58);
  }

  _getForecast = (lat, lon) => {
    // const API_KEY = 'c888268666fb979927a97be6d20393f6'; // openweathermap api key
    // const API_KEY = 'cd079b371e7e8b3e3c87bc0ee5965a51'; // dark sky weather api
    const baseURL =
      "https://api.darksky.net/forecast/cd079b371e7e8b3e3c87bc0ee5965a51/";
    const url = `${baseURL}${lat},${lon}?exclude=minutely,hourly,flags`;

    // const baseURL =  "https://api.darksky.net/forecast/cd079b371e7e8b3e3c87bc0ee5965a51/";
    // const url = `${baseURL}${lat},${lon}?exclude=minutely,hourly,flags`;
    // const baseURL = 'http://api.openweathermap.org/data/2.5/forecast/daily?appid=c888268666fb979927a97be6d20393f6&cnt=7&units=imperial';//OpenweatherMap baseURL
    // const url = `${baseURL}&lat=${lat}&lon=${lon}`; //URL for openweathermap request
    // use below block for openweathermap
    // return fetch(url, {
    // method: 'GET'
    //  }).then(res => res.json())
    //   .then(res => {
    //     this.setState({
    //       weatherData: res.list
    //     })
    //   });
    // console.log(lat,lon);
    return fetch(url) // this block is for dark sky api request
      .then(res => res.json())
      .then(res => {
        this.setState({
        //   weatherData: res.daily.data,
          weatherDataCurrent: res.currently
        });
      });
  };

  displayLocation = (lat, lon) => {
    const baseLoc = "https://maps.googleapis.com/maps/api/geocode/json?latlng=";
    const loc = `${baseLoc}${lat},${lon}&key=AIzaSyDv0PKWmJisHceXO9D3oINiHyyyxmCRafQ`;
    // curLocation gets only the location equal to the fetched lat and lon
    return fetch(loc) // this block converts lat, lon of current location to a street address
    .then(res => res.json())
    .then(resJson => {
      this.setState({
      curLocation: resJson.plus_code.compound_code,
      });
    });
    
  }

  getIconImage = iconId => {
    switch (iconId) {
      case "clear-day":
        imageUrl = require("../../../assets/images/weatherIcons/clear-day.png");
        break;
      case "clear-night":
        imageUrl = require("../../../assets/images/weatherIcons/clear-night.png");
        break;
      case "cloudy-day":
      case "mostly-cloudy":
        imageUrl = require("../../../assets/images/weatherIcons/cloudy-day.png");
        break;
      case "cloudy-night":
        imageUrl = require("../../../assets/images/weatherIcons/cloudy-night.png");
        break;
      case "partly-cloudy-day":
        imageUrl = require("../../../assets/images/weatherIcons/partly-cloudy-day.png");
        break;
      case "partly-cloudy-night":
        imageUrl = require("../../../assets/images/weatherIcons/partly-cloudy-night.png");
        break;
      case "rain":
        imageUrl = require("../../../assets/images/weatherIcons/rain.png");
        break;
      case "sleet":
        imageUrl = require("../../../assets/images/weatherIcons/sleet.png");
        break;
      case "fog":
        imageUrl = require("../../../assets/images/weatherIcons/fog.png");
        break;
      case "wind":
        imageUrl = require("../../../assets/images/weatherIcons/wind.png");
        break;

      case "snow":
        imageUrl = require("../../../assets/images/weatherIcons/snowflake.png");
        break;
      case "thunderstorm":
        imageUrl = require("../../../assets/images/weatherIcons/thunderstorm.png");
        break;
      default:
        imageUrl = require("../../../assets/images/weatherIcons/clear-day.png");
    }
    return imageUrl;
  };

  getBackImage = iconId => {
    switch (iconId) {
      case "clear-day":
      case "overcast":
        imageBg = require("../../../assets/images/bg/clear1.jpg");
        break;
      case "clear-night":
        imageBg = require("../../../assets/images/bg/clearNightBmore.jpg");
        break;
      case "cloudy-day":
      case "mostly-cloudy":
      case "partly-cloudy-day":
        // imageBg = require("../../../assets/images/bg/cloud.jpg");
        imageBg = require("../../../assets/images/bg/clear1.jpg");
        break;
      case "cloudy-night":
        imageBg = require("../../../assets/images/bg/cloudyBmore.jpg");
        break;
      case "partly-cloudy-night":
        imageBg = require("../../../assets/images/bg/night.jpg");
        break;
      case "rain":
        imageBg = require("../../../assets/images/bg/rain2.jpg");
        break;
      case "sleet":
        imageBg = require("../../../assets/images/bg/sleet1.jpg");
        break;
      case "fog":
        imageBg = require("../../../assets/images/bg/haze.jpg");
        break;
      case "wind":
        imageBg = require("../../../assets/images/bg/sleet1.jpg");
        break;

      case "snow":
        imageBg = require("../../../assets/images/bg/snow1.png");
        break;
      case "thunderstorm":
        imageBg = require("../../../assets/images/bg/thunderstorm.jpg");
        break;
      default:
        imageBg = require("../../../assets/images/bg/clear1.jpg");
    }
    return imageBg;
  };

  getLocation = () => {
    navigator.geolocation.getCurrentPosition(
      posData => {
        this._getForecast(
          posData.coords.latitude,
          posData.coords.longitude
        )
        this.displayLocation(
          posData.coords.latitude,
          posData.coords.longitude
        )
      }
    );
    
  };


  render() {
    const iconId = this.state.weatherDataCurrent.icon;
    curTemp = Math.round(this.state.weatherDataCurrent.temperature * 10) / 10;
    imageUrl = this.getIconImage(iconId);
    imageBg = this.getBackImage(iconId);
    
    // console.log(this.state);

    let view =  (
      <Today
        data={this.state.weatherDataCurrent}
        iconId={this.state.weatherDataCurrent.icon}
        myLocation = { this.state.curLocation}
      />
    );

    return (
      <Container style={styles.container}>
        <ImageBackground style={styles.imageContainer} source={imageBg}>
          <Content>
            <View style={styles.horizontalLine} />

            {view}
          </Content>
        </ImageBackground>
      </Container>
    );
  }
}

// export default Weather;
