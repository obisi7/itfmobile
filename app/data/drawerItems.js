
export const drawerItems = [
    {
      
      name: 'ITF Official Website',
      route: 'Home',
      icon: 'home',
    },
    {
      name: 'Apply for training',
      route: 'Training',
      icon: 'lightbulb-o',
    },
    {
      name: 'Contact Us',
      route: 'FeedbackScreen',
      icon: 'comments',
    },
    {
      name: 'Logout',
      route: 'Auth',
      icon: 'sign-out',
    },
  
  ]