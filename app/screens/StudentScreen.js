import React, { Component, Fragment } from 'react';
import { StyleSheet, SafeAreaView, Image, 
  Linking, Dimensions, Platform, StatusBar, 
  TouchableOpacity, View, Text, AppState, 
} from 'react-native';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';
import {
  Grid,
  Col,
  Row,
} from 'native-base';
// import FitImage from 'react-native-fit-image';
// import { scrape } from '../utils/ScrapeTestimonial';
import { Header, Footer } from '../components/widgets';
import ModalPopup from '../components/radio/modalPopup';
import ScrapeData from '../utils/ScrapeData';


const deviceHeight = Dimensions.get("window").height;

{/* <Ionicons name="md-checkmark-circle" size={32} color="green" /> */}
const studentImages = [
  require('../../assets/images/current-students.jpg'),
  require('../../assets/images/student1.jpg'),
  require('../../assets/images/student2.jpg'),
  // require('../../assets/images/student3.jpg'),
  // require('../../assets/images/student4.jpg'),
];
const appBackgroundImage = require("../../assets/images/morganBg.png");


export class StudentScreen extends Component {

  constructor(props) {
    super(props);

    this.initialState = {
      homeview: "homeview",
      setModalVisible: false,
      modalTitle: '',
      modalHeaderColor: '#f47937',
      modalURL: '',
      modalBannerImage: null,
    }
    this.state = this.initialState;
  }
  
  _handleItemDataOnPress = (link, title, headColor, modalClicked) => {
    // const headerImage = `require('../../assets/images/${headImage}')`
    
    this.setState({
      setModalVisible: true,
      modalURL: link,
      modalTitle: title,
      modalHeaderColor: headColor,
      modalBannerImage: modalClicked,
      // modalBannerImage: `'../../assets/images/${headImage}'`,
    });
    
  };
  _handleModalClose = () => {
    this.setState({
      setModalVisible: false,
      modalURL: '',
      modalTitle: '',
      modalHeaderColor: '#f47937',
      modalBannerImage: null,
    });
  };

_renderHomeView = ( ) => {

    return (
      <Fragment>
      
      {/* <SafeAreaView style={{ flex: 0, backgroundColor: '#ff0000' }} forceInset={{ top: 'always' }} /> */}
      
      <SafeAreaView style={{ flex: 1, backgroundColor: '#006600' }} > 
      {/* <ImageBackground source={ appBackgroundImage } style={{width: '100%', height: '100%'}}> */}
      <StatusBar barStyle='light-content' />
      <View style={styles.container}>
        {/* <Header 
            headerText={'Student Portal'}
            iconLeftName= 'menu'
            iconRightName= 'login'
            iconLeftStyle= {{ paddingHorizontal: 10, fontSize: 30, color: 'white',  }}  leftIconPress={() => this.props.navigation.openDrawer()}  
            iconRightStyle= {{ paddingHorizontal: 10, fontSize: 30, color: 'white',  }}  rightIconPress={() => this._showMessage()} 
            backgroundColor = {'#006600'}
            color = { '#fff'}
            style={[ {height: 90}, styles.androidHeader ]}
        /> */}
        <View style={styles.horizontalLine} />
        <View style={{ marginBottom: 30 }}>
          {/* <Image
            source={
                require('../../assets/images/itfLearn1.png')
            }
            style={styles.contentImageStyle}
          /> */}
          <Image
            source={
              studentImages[Math.floor(Math.random() * studentImages.length)]
            }
            // style={styles.fitImage}
            style={styles.contentImageStyle}
          />
        </View>
        <View style={[ styles.container, { backgroundColor: this.props.backgroundColor } ]}>

        <Grid>
          <Row style={styles.row}>
            <Col style={styles.col}>
              <TouchableOpacity>
                <Icon
                  name="library-books"
                  style={styles.icon}
                  onPress={() => 
                    this._handleItemDataOnPress('http://morganstatebears.com', 'Morgan State Athletics', '#006600')}
                  
                  // onPress={() => this.props.navigation.navigate('Events')}
                />
              </TouchableOpacity>
              <Text numberOfLines={1} style={styles.iconText}>
                Courses
              </Text>
            </Col>

            <Col style={styles.col}>
              <TouchableOpacity>
                <Icon
                  name="rss"
                  style={styles.icon}
                  onPress={() => this.props.navigation.navigate('News')}
                />
                <Text numberOfLines={1} style={styles.iconText}>
                  News
                </Text>
              </TouchableOpacity>
            </Col>
            <Col style={styles.col}>
              <Icon
                name="image-multiple"
                style={styles.icon}
                onPress={() => this.props.navigation.navigate('Photos')}
              />
              <Text numberOfLines={1} style={styles.iconText}>
                Photos
              </Text>
            </Col>
            <Col style={styles.col}>
              <TouchableOpacity>
                <Icon
                  name="video"
                  style={styles.icon}
                  onPress={() => this.props.navigation.navigate('Videos')}
                />
                <Text numberOfLines={1} style={styles.iconText}>
                  Videos
                </Text>
              </TouchableOpacity>
            </Col>
          </Row>

          <Row style={styles.row}>
            <Col style={styles.col}>
              <TouchableOpacity>
                <Icon
                  name="office"
                  style={styles.icon}
                  onPress={() => 
                    // this.props.navigation.navigate('ScrapeWeb')}
                    this._handleItemDataOnPress('https://gateway.morgan.edu/', 'My Morgan State','#006600')}
                />
              </TouchableOpacity>
              <Text numberOfLines={1} style={styles.iconText}>
                Program
              </Text>
            </Col>
            <Col style={styles.col}>
              <TouchableOpacity>
                <Icon
                  name="weather-rainy"
                  style={styles.icon}
                  onPress={() => this.props.navigation.navigate('Weather')}
                />
                <Text numberOfLines={1} style={styles.iconText}>
                  Weather
                </Text>
              </TouchableOpacity>
            </Col>
            <Col style={styles.col}>
              <TouchableOpacity>
                <Icon
                  name="radio"
                  style={styles.icon}
                  onPress={() => 
                    this._handleItemDataOnPress('http://radio.wazobiafm.com/#', 'Wazobia: Naija  Radio', '#006600')}
                    // this._handleItemDataOnPress('http://amber.streamguys.com:4020/live', 'WEAA: Morgan Radio', '#006600')}
                />
                <Text numberOfLines={1} style={styles.iconText}>
                  Radio
                </Text>
              </TouchableOpacity>
            </Col>
            <Col style={styles.col}>
              <Icon
                name="pen"
                style={styles.icon}
                onPress={() =>
                  this._handleItemDataOnPress('http://www.morgan.edu/library', 'ESR Library', '#006600')
                }
              />
              <Text numberOfLines={1} style={styles.iconText}>
                Apply
              </Text>
            </Col>
            
          </Row>


          <Row style={styles.row}>
            <Col style={styles.col}>
              <Icon
                name="alert"
                style={styles.icon}
                onPress={() => 
                  this._handleItemDataOnPress('http://www.morgan.edu/mobilealerts', 'Mobile Alert System', '#006600')}
              />
              <Text numberOfLines={1} style={styles.iconText}>
                Alerts
              </Text>
            </Col>
            <Col style={styles.col}>
              <Icon
                active
                name="phone-outgoing"
                style={styles.icon}
                onPress={() =>
                  this._handleItemDataOnPress('http://www.morgan.edu/contactus/', 'Contact Us', '#006600')
                }
              />
              <Text numberOfLines={1} style={styles.iconText}>
                Contacts
              </Text>
            </Col>
            <Col style={styles.col}>
            <Icon
                active
                name="map-marker"
                style={styles.icon}
                onPress={() =>
                  this._handleItemDataOnPress('http://map.morgan.edu', 'Morgan Campus Map', '#006600')
                }
              />
              <Text numberOfLines={1} style={styles.iconText}>
                Map
              </Text>
            </Col>
            <Col style={styles.col}>
              <Icon
                active
                name="eye"
                style={styles.icon}
                onPress={() =>
                  this._handleItemDataOnPress('http://www.morgan.edu/apply_now.html', 'Apply to Morgan', '#006600')
                  // scrape()
                }
              />
              <Text numberOfLines={1} style={styles.iconText}>
                Testimony
              </Text>
            </Col>
          </Row>

        </Grid>
          
      </View>
      <ModalPopup
        showModal={this.state.setModalVisible}
        url={this.state.modalURL}
        title={this.state.modalTitle}
        headerBgColor={this.state.modalHeaderColor}
        onClose={this._handleModalClose}
        modalType={this.state.modalBannerImage}
      />
        {/* <Footer /> */}
      </View>
      {/* </ImageBackground> */}
      </SafeAreaView>
      </Fragment>
      
    )

  }

  _renderTestView() {
    return (
      <Fragment>
      
      <SafeAreaView style={{ flex: 0, backgroundColor: '#006600' }} forceInset={{ top: 'always' }} />
      
      <SafeAreaView style={{ flex: 1, backgroundColor: '#ff0000' }} > 
      <StatusBar barStyle='light-content' />
      <View style={styles.container}>
        <Header 
          headerText={'My ITF Mobile Special'} 
          backgroundColor = {'#006600'}
          color = { '#fff'}
          style={[ {height: 90}, styles.androidHeader ]}
          />
        <Footer />
      </View>
      </SafeAreaView>
      </Fragment>
    )
  }

  render() {
    let view = (this.state.homeview == "testview")
      ? this._renderTestView()
      : this._renderHomeView();

    return (
      // <ImageBackground source={ appBackgroundImage } style={{width: '100%', height: '100%'}}>

        <View style={{ flex:1,  }}>
          {view}
          {/* <Text> Calculator Screen </Text> */}
        </View>
      // </ImageBackground>
    )
  }

}

const styles = StyleSheet.create({
  androidHeader: {
    ...Platform.select({
      android: {
        paddingTop: StatusBar.currentHeight,
      }
    })
  },
  container: {
    flex: 1,
    width: null,
    backgroundColor: 'transparent',
  },
  horizontalLine: {
    borderBottomColor: '#000',
    borderBottomWidth: 1,
  },
  iconContainer: {
    flexDirection: 'row',
    flexWrap: 'wrap',
    justifyContent: 'space-around',
    paddingLeft: 15,
  },
  iconText: {
    fontSize: 16,
    color: '#fff',
    marginTop: 2,
  },
  icon: {
    width: 40,
    height: 40,
    justifyContent: 'center',
    color: '#fff',
    fontSize: 30,
  },
  col: {
    alignItems: 'center',
    paddingHorizontal: 3,
  },
  row: {
    paddingBottom: 30,
  },
  contentImageStyle: {
    // alignSelf: "stretch",
    // resizeMode: 'contain',
    resizeMode: 'cover',
    height: deviceHeight / 3.5,
    width: null,
    // position: "relative",
    marginBottom: 10
  },
})