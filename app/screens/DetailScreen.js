import React, { Component } from "react";
import { 
    View,
    Text,
    StyleSheet
} from "react-native";

export class DetailScreen extends Component {
    render() {
        return (
            <View style={styles.container}>
                <Text>DetailScreen</Text>
            </View>
        );
    }
}
// export default DetailScreen;

const styles = StyleSheet.create({
    container: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center'
    }
});