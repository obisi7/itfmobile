import React, { Component } from 'react';
import { Text, View, StyleSheet, SafeAreaView } from 'react-native';

export class SettingScreen extends Component {
    
    render(){
        return (
            <SafeAreaView style={{ flex: 1, backgroundColor: '#f8f8f8' }} >
            <View style={styles.container}>
                <Text >Config App Settings Here  </Text>
            </View>
            </SafeAreaView>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center',
        // backgroundColor: '#e84e35'
    }
});