import React, { Component } from 'react';
import { Image, View, StyleSheet, Dimensions,Platform,} from 'react-native';
import Icon from 'react-native-vector-icons/FontAwesome';
import {
  Content,
  Text,
  List,
  ListItem,
  Container,
  Left,
} from 'native-base';

import ModalPopup from '../screens/modalPopup';
import { drawerItems } from '../data/drawerItems';


const deviceHeight = Dimensions.get('window').height;


export class DrawerMenu extends Component {
  
  constructor(props) {
    super(props);
    this.state = {
      shadowOffsetWidth: 1,
      shadowRadius: 4,
      setModalVisible: false,
      modalTitle: '',
      modalHeaderColor: '#f47937',
      modalURL: '',
      // modalBannerImage: null,
    };
  }
  _handleItemDataOnPress = (link, title, headColor, modalClicked) => {
    // const headerImage = `require('../../assets/images/${headImage}')`
    
    this.setState({
      setModalVisible: true,
      modalURL: link,
      modalTitle: title,
      modalHeaderColor: headColor,
      // modalBannerImage: `'${modalClicked}'`,
      // modalBannerImage: `'../../assets/images/${headImage}'`,
    });
    
  };
  _handleModalClose = () => {
    this.setState({
      setModalVisible: false,
      modalURL: '',
      modalTitle: '',
      modalHeaderColor: '#c62020',
      modalBannerImage: null,
    });
  };

  _switcher = (choice) => {
    switch(choice) {
      case 'Home': {
        this._handleItemDataOnPress('http://www.itf.gov.ng', 'Visit ITF Website ', '#c62020')
      }
      break;
      case 'Training': {
        this._handleItemDataOnPress('https://www.itf.gov.ng/programmes.php#', 'Apply for training', '#1b4383')
      }
      break;
      // case 'Weaa': {
      //   this._handleItemDataOnPress('http://amber.streamguys.com:4020/live', 'WEAA Radio Station', '#1b4383', 'radio')
      // }
      // break;
      default: this.props.navigation.navigate(choice);

    }
  }

  render() {
    return (
      <Container>
        
        <Content bounces style={{ flex: 1, backgroundColor: '#000', top: -1 }}>
        <View style={styles.profileContainer}>
          <Image
              source={
                  require('../../assets/images/itfDGClear.png')
              }
              style={styles.contentImageStyle}
          />
          <Text style={styles.profileText}> Sir Joseph N. Ari, DG </Text>
          <Text />
        </View>
          <List
            dataArray={drawerItems}
            renderRow={data =>
            (<ListItem 

              onPress={() => {

                this._switcher(data.route);
              
            }}>
              <Left>
                <Icon active name={data.icon} style={styles.sidebarIcon} />
                <Text style={styles.text}>
                  {data.name}
                </Text>
              </Left>

            </ListItem>)}
          /> 
        </Content>
        <ModalPopup
          showModal={this.state.setModalVisible}
          url={this.state.modalURL}
          title={this.state.modalTitle}
          headerBgColor={this.state.modalHeaderColor}
          onClose={this._handleModalClose}
          modalType={this.state.modalBannerImage}
        />
      </Container>
    );
  }
}

const styles = StyleSheet.create ({
  profileContainer: {
    // height: deviceHeight / 3.0,
    justifyContent:'center',
    alignItems:'center',
    width: null,
    position: 'relative',
    marginBottom: 20,
    backgroundColor: '#c62020', //itf's red color
  },
  sidebarIcon: {
    fontSize: 21,
    color: '#fff',
    lineHeight: Platform.OS === 'android' ? 21 : 25,
    backgroundColor: 'transparent',
    width: 30,
    // alignSelf: 'center',
  },
  text: {
    fontWeight: Platform.OS === 'ios' ? '500' : '400',
    fontSize: 16,
    marginLeft: 20,
    color:'#fff'
  },
  profileText: {
    fontWeight: Platform.OS === 'ios' ? '500' : '400',
    fontSize: 16,
    alignItems: 'center',
    color:'#fff'
  },
  profileImage: {
    marginTop: 60,
    resizeMode: 'cover',
    justifyContent: 'center',
    // width: 80,
    // height: 80,
    // borderRadius: 40,
  },
  contentImageStyle: {
    alignSelf: "stretch",
    // resizeMode: 'cover',
    height: deviceHeight / 3.5,
    width: null,
    position: "relative",
    marginBottom: 10
  },

})
