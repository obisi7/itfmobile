import React, { Component } from 'react';
import { Image } from 'react-native';
import Icon from 'react-native-vector-icons/FontAwesome';
import {
  Content,
  Text,
  List,
  ListItem,
  Container,
  Left,
} from 'native-base';
// import { connect } from "react-redux";

// import { watchCoverImageData } from '../store/actions';
import styles from './styleSideBar';
// import { drawerCovers } from '../data/drawerCovers';
import { drawerItems } from '../data/drawerItems';

export class SideBar extends Component {
  
  constructor(props) {
    super(props);
    this.state = {
      shadowOffsetWidth: 1,
      shadowRadius: 4,
      // isModalVisible: false,
    };
  }

//   componentWillMount(){
//     this.props.watchCoverImageData();
// }

  render() {
    // const { coverImageData } = this.props;
    return (
      <Container>
        
        <Content bounces style={{ flex: 1, backgroundColor: '#006600', top: -1 }}>
        <Image
            source={
                require('../../assets/images/bg3.png')
            }
            style={styles.drawerCover}
          />
        {/* <Image 
            source={{ uri: coverImageData[Math.floor(Math.random() * coverImageData.length)] }}
            style={styles.drawerCover}
        /> */}
          <List
            dataArray={drawerItems}
            renderRow={data =>
            (<ListItem 

            onPress={() => {

              this.props.navigation.navigate(data.route);
              
            }}>
              <Left>
                <Icon active name={data.icon} style={{ color: '#ffff', fontSize: 26, width: 30 }} />
                <Text style={styles.text}>
                  {data.name}
                </Text>
              </Left>

            </ListItem>)}
          /> 
        </Content>
      </Container>
    );
  }
}

// const mapStateToProps = ({ appdata }) => {
//   const {  coverImageData } = appdata;
//   return { 
//     coverImageData: coverImageData,
//   };
// }

// const mapDispatchToProps = (dispatch) => {
//   return { 
//     watchCoverImageData: () => dispatch(watchCoverImageData()),
//   };
// }

