import React, { Component, Fragment } from 'react';
import { StyleSheet, SafeAreaView, Image, Dimensions, Platform, StatusBar, ImageBackground, PanResponder, View, Text } from 'react-native';
import { Display, Header, Footer } from './../components/widgets';

{/* <Ionicons name="md-checkmark-circle" size={32} color="green" /> */}

const deviceHeight = Dimensions.get("window").height;
const randomImages = [
  require('../../assets/images/bg0.png'),
  require('../../assets/images/bg1.png'),
  require('../../assets/images/bg2.png'),
  require('../../assets/images/bg3.png'),
  require('../../assets/images/bg4.png'),
];
const appBackgroundImage = require("../../assets/images/morganBg.png");


export  class HomeScreen extends Component {

  constructor(props) {
    super(props);

    this.initialState = {
      displayValue: "0",
      result: "0",
      calculationDone: false,
      homeview: "homeview",
    }
    this.state = this.initialState;
  }

_renderHomeView = ( ) => {

    return (
      <Fragment>
      
      <SafeAreaView style={{ flex: 0, backgroundColor: '#006600' }} forceInset={{ top: 'always' }} />
      
      <SafeAreaView style={{ flex: 1, backgroundColor: '#ff0000' }} > 
      <ImageBackground source={ appBackgroundImage } style={{width: '100%', height: '100%'}}>
      <StatusBar barStyle='light-content' />
      <View style={styles.container}>
        <Header 
          headerText={'My ITF Mobile'} 
          backgroundColor = {'#006600'}
          color = { '#fff'}
          style={[ {height: 90}, styles.androidHeader ]}
          />
        <View style={styles.horizontalLine} />
        <View>
          <Image
            source={
              randomImages[Math.floor(Math.random() * randomImages.length)]
            }
            style={styles.contentImageStyle}
          />
        </View>
        <Display />
        {/* <Footer /> */}
      </View>
      </ImageBackground>
      </SafeAreaView>
      </Fragment>
      
    )

  }

  _renderTestView() {
    return (
      <Fragment>
      
      <SafeAreaView style={{ flex: 0, backgroundColor: '#006600' }} forceInset={{ top: 'always' }} />
      
      <SafeAreaView style={{ flex: 1, backgroundColor: '#ff0000' }} > 
      <StatusBar barStyle='light-content' />
      <View style={styles.container}>
        <Header 
          headerText={'My ITF Mobile Special'} 
          backgroundColor = {'#006600'}
          color = { '#fff'}
          style={[ {height: 90}, styles.androidHeader ]}
          />
        <Footer />
      </View>
      </SafeAreaView>
      </Fragment>
    )
  }

  render() {
    let view = (this.state.homeview == "testview")
      ? this._renderTestView()
      : this._renderHomeView();

    return (
      // <ImageBackground source={ appBackgroundImage } style={{width: '100%', height: '100%'}}>

        <View style={{ flex:1,  }}>
          {view}
          {/* <Text> Calculator Screen </Text> */}
        </View>
      // </ImageBackground>
    )
  }

}

const styles = StyleSheet.create({
  androidHeader: {
    ...Platform.select({
      android: {
        paddingTop: StatusBar.currentHeight,
      }
    })
  },
  container: { 
    flex: 1, 
    // paddingVertical: 5, 
    // backgroundColor: "black" // comment out to show background image
  },
  horizontalLine: {
    borderBottomColor: '#000',
    borderBottomWidth: 1,
  },
  
  contentImageStyle: {
    // flex: 1,
    // width: null,
    // height: 150,
    // marginBottom: 40,
    alignSelf: "stretch",
    // resizeMode: 'cover',
    height: deviceHeight / 3.5,
    width: null,
    position: "relative",
    marginBottom: 10
  },
})