import React, { Component } from "react";
import { 
    View,
    Text,
    StyleSheet,
    SafeAreaView,
} from "react-native";

export class ProfileScreen extends Component {
    render() {
        return (
            <SafeAreaView style={{ flex: 1, backgroundColor: '#f8f8f8' }} >
                <View style={styles.container}>
                    <Text>My Profile Here </Text>
                </View>
            </SafeAreaView>
        );
    }
}
// export default ProfileScreen;

const styles = StyleSheet.create({
    container: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center'
    }
});