import React, { Component, Fragment } from 'react';
import { StyleSheet, Image, Linking,
  Dimensions, Platform, StatusBar, 
  ImageBackground, TouchableOpacity, View, Text } from 'react-native';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';
import { SafeAreaView } from 'react-navigation';
import {
  Grid,
  Col,
  Row,
} from 'native-base';

import ModalPopup from '../components/radio/modalPopup';
import {  Header, Footer } from '../components/widgets';

const deviceHeight = Dimensions.get("window").height;
const randomImages = [
  require('../../assets/images/itfBan1.jpg'),
  require('../../assets/images/itfBan2.jpg'),
  require('../../assets/images/itfBan3.jpg'),
  require('../../assets/images/itfBan4.jpg'),
  require('../../assets/images/itfBan5.jpg'),
  require('../../assets/images/itfStaff1.jpg'),
  require('../../assets/images/itfJosBldg.jpg'),
];

// const leftLogo = require('../../assets/images/holmesHall.png')
const rightLogo = require('../../assets/images/itfLogo.png')
// const rightLogo = require('../../assets/images/bearIcon2.png')
const appBackgroundImage = require("../../assets/images/itfBackground.png");
// const appBackgroundImage = require("../../assets/images/morganBg.png");


export  class WelcomeScreen extends Component {

  constructor(props) {
    super(props);

    this.initialState = {
      // homeview: "homeview",
      setModalVisible: false,
      modalTitle: '',
      modalURL: '',
      modalHeaderColor: '#f47937',
    }
    this.state = this.initialState;
  }


  _handleItemDataOnPress = (link, title, headColor) => {
    this.setState({
      setModalVisible: true,
      modalURL: link,
      modalTitle: title,
      modalHeaderColor: headColor,
    });
  };
  _handleModalClose = () => {
    this.setState({
      setModalVisible: false,
      modalURL: '',
      modalTitle: '',
      modalHeaderColor: '#f47937',
    });
  };

//   openMyURL = (url) => {
//     Linking.canOpenURL(url).then(supported => {
//         if (!supported) {
//             console.log('Can\'t handle url: ' + url);
//         } else {
//             return Linking.openURL(url);
//         }
//     }).catch(err => console.error('An error occurred', err))
// }
  _renderWelcomeView = ( ) => {

    return (
      <Fragment>
      
      <SafeAreaView style={{ flex: 0, backgroundColor: '#c62020' }}  />
      
      <SafeAreaView style={{ flex: 1,  }} > 
      <ImageBackground 
        source={ appBackgroundImage } 
        style={{width: '100%', height: '100%'}}
        // blurRadius={2}
      >
      <StatusBar barStyle='light-content' />
        <View style={styles.container}>
        <Header 
          headerText={'ITF Nigeria'}
          // iconLeftName= 'menu'
          iconFb= 'facebook'
          iconTwitter= 'twitter'
          iconYoutube= 'youtube'
          iconFbStyle= {{  paddingHorizontal:5, fontSize: 24, color: 'white',  }}  fbIconPress={() => this._handleItemDataOnPress('https://www.facebook.com/itfniger', 'Our Facebook Sphere', '#610dc7')}    
          iconTwitterStyle= {{  paddingHorizontal:5, fontSize: 24, color: 'white',  }}  twitterIconPress={() => this._handleItemDataOnPress('https://twitter.com/ITFNigeria', 'Our Twitter Sphere', '#006600')}    
          // iconInstaStyle= {{  paddingHorizontal:5, fontSize: 24, color: 'white',  }}  instaIconPress={() => this._handleItemDataOnPress('https://www.youtube.com/channel/UCFDbjGkGmL6rdvccHyfK-Wg', 'Our Youtube Cahnnel', '#e91e63')}    
          iconYoutubeStyle= {{  paddingHorizontal:5, fontSize: 24, color: 'white',  }}  youTubeIconPress={() => this.props.navigation.navigate('Videos')}    
          // iconRightStyle= {styles.iconRightStyle} 
          // imageLeft={leftLogo}
          iconRightName= 'menu'
          iconRightStyle= {{  paddingHorizontal:5, fontSize: 30, color: 'white',  }}  rightIconPress={() => this.props.navigation.openDrawer()} 
          imageRight = {rightLogo}
          // imageHeaderLeft = {styles.imageHeaderLeft}
          imageHeaderRight = {styles.imageHeaderRight}
          backgroundColor = {'#c62020'}
          color = { '#fff'}
          style={[ {height: 90}, styles.androidHeader ]}
        />
          <View style={styles.horizontalLine} />
          <View>
            <Image
              source={
                randomImages[Math.floor(Math.random() * randomImages.length)]
              }
              style={styles.contentImageStyle}
            />
          </View>
          <View style={{ marginBottom: 20 }} />
          <Grid>
            <Row style={styles.row}>
              <Col style={styles.col}>
                <TouchableOpacity>
                  <Icon
                    name="briefcase-edit"
                    style={styles.icon}
                    onPress={() => this.props.navigation.navigate('Staff')}
                  />
                </TouchableOpacity>
                <Text numberOfLines={1} style={styles.iconText}>
                  Staff
                </Text>
              </Col>

              <Col style={styles.col}>
                <TouchableOpacity>
                  <Icon
                    
                    name="chair-school"
                    style={styles.icon}
                    onPress={() => this.props.navigation.navigate('Student')}
                  />
                
                  <Text numberOfLines={1} style={styles.iconText}>
                    Student
                  </Text>
                </TouchableOpacity>
              </Col>
            </Row>
            <Row style={styles.row}>
              <Col style={styles.col}>
                <TouchableOpacity>
                  <Icon
                    name="human-male-female"
                    // name="book-open-page-variant"
                    style={styles.icon}
                    onPress={() => this.props.navigation.navigate('Guest')}
                  />
                
                  <Text numberOfLines={1} style={styles.iconText}>
                    Guest
                  </Text>
                </TouchableOpacity>
              </Col>
              <Col style={styles.col}>
                <TouchableOpacity>
                  <Icon
                    name="school"
                    style={styles.icon}
                    onPress={() => this.props.navigation.navigate('Alumni')}
                  />
                  <Text numberOfLines={1} style={styles.iconText}>
                    Alumni
                  </Text>
                </TouchableOpacity>
              </Col>
            </Row>
          </Grid>
          {/* <Display /> */}
          {/* <Footer /> */}
        </View>
        <ModalPopup
          showModal={this.state.setModalVisible}
          url={this.state.modalURL}
          title={this.state.modalTitle}
          headerBgColor={this.state.modalHeaderColor}
          onClose={this._handleModalClose}
        />
      </ImageBackground>
      </SafeAreaView>
      </Fragment>
      
    )

  }


  render() {

    return (

        <View style={{ flex:1,  }}>
          {this._renderWelcomeView()}
        </View>
    )
  }

}

const styles = StyleSheet.create({
  androidHeader: {
    ...Platform.select({
      android: {
        paddingTop: StatusBar.currentHeight,
      }
    })
  },
  container: { 
    flex: 1, 
    // paddingVertical: 5, 
    // backgroundColor: "black" // comment out to show background image
  },
  horizontalLine: {
    borderBottomColor: '#000',
    borderBottomWidth: 1,
  },
  
  contentImageStyle: {
    alignSelf: "stretch",
    // resizeMode: 'cover',
    height: deviceHeight / 3.5,
    width: null,
    position: "relative",
    marginBottom: 10
  },

  imageHeaderLeft: {
    resizeMode: 'contain', 
    paddingHorizontal: 5, 
    width: 50, 
    height: 50,
    borderRadius: 25,
    backgroundColor: '#fff',
    borderWidth: 2,
    
    borderColor: '#fff'
  },
  imageHeaderRight: {
    resizeMode: 'contain', 
    marginRight: 15, 
    width: 50, 
    height: 50,
    borderRadius: 25,
    borderWidth: 2,
    borderColor: '#fff'
  },
  iconRightStyle: {
    paddingHorizontal: 10, 
    fontSize: 30, 
    color: 'white', 
    alignSelf: 'flex-end' 
  },
  iconText: {
    fontSize: 20,
    color: '#fff',
    marginTop: 2,
    alignItems: 'center',
    justifyContent: 'center',
  },
  icon: {
    // width: 60,
    // height: 60,
    justifyContent: 'center',
    alignItems: 'center',
    color: '#fff',
    fontSize: 50,
  },
  col: {
    alignItems: 'center',
    justifyContent: 'center',
    // paddingHorizontal: 3,
  },
  row: {
    justifyContent: 'center',
    alignItems: 'center',
    // paddingBottom: 20,
  },
})