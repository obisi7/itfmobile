/* Mobile app developed as a teaching tool 
  by Prof. Bisi Oladipupo. No intent to release as an alternate
  or competing app to university's mobile app.
  Favorite colors: Morgan Blue: #1b4383, Orange: #f47937
  Morgan Gold: #ffcc33
  Other fav colors are: Dark Green: #006600, Dark Blue: #000066
  
*/

import React, { Component } from 'react';
import { View, Text, StyleSheet,  } from 'react-native';

import AppNav from './AppNav';


export default class App extends Component {

  render() {
    return (
      <AppNav />
      // <WelcomeScreen />
    )
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },
})

// export default App;