// Below code is trying to put tabs on weather screen for navigation. Not working yet
import React, { Component } from "react";
import { StyleSheet,  } from "react-native";

import { 
    createStackNavigator,
    createSwitchNavigator, 
    createDrawerNavigator, 
    createAppContainer,
    createBottomTabNavigator,
} from 'react-navigation';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';

import { WelcomeScreen } from "./screens/WelcomeScreen";
import { StaffScreen } from "./screens/StaffScreen";
import { GuestScreen } from "./screens/GuestScreen";
import { AlumniScreen } from "./screens/AlumniScreen";
import { ProfileScreen } from "./screens/ProfileScreen";
import { SettingScreen } from "./screens/SettingScreen";
import { DrawerMenu } from "./screens/DrawerMenu";
import { StudentScreen } from "./screens/StudentScreen";
import  { Videos } from "./components/videos";
import { Photos }   from "./components/photos";
import { News }   from "./components/news";
import { MyToday }   from "./components/weather/myToday";
import { MyForecast }   from "./components/weather/myForecast";
import { Search }   from "./components/weather/search";
import { ScrapeData } from './utils/ScrapeData';
// const headerIcon = (Platform.OS) == 'ios'? 'headerRight' : 'headerLeft';
class AppNav  extends Component {

    render() {
        return (
            <AppContainer />
        );
    }
}

export default AppNav;

const TodayStack = createStackNavigator({
  Today: {
    screen: MyToday,
    navigationOptions: ({ navigation }) => {
      return {
        // headerTitle: 'Today\'s Weather',
        //   headerStyle: {
        //     backgroundColor: '#e84e35',
        //   },
        //   headerTintColor: '#fff',
        //   headerTitleStyle: {
        //     fontWeight: 'bold',
        //   },
        header: null,
      }
    },
  },
  
});

const ForecastStack = createStackNavigator({
  Forecast: {
    screen: MyForecast,
    
    navigationOptions: ({ navigation }) => {
      return {
        
        // headerTitle: '7-Day Forecast',
        //   headerStyle: {
        //     backgroundColor: '#e84e35',
        //   },
        //   headerTintColor: '#fff',
        //   headerTitleStyle: {
        //     fontWeight: 'bold',
        //   },
        header: null,
      }
    },
  },
  
});
const SearchStack = createStackNavigator({
  Search: {
    screen: Search,
    navigationOptions: ({ navigation }) => {
      return {
        headerTitle: 'Do Search',
          headerStyle: {
            backgroundColor: '#e84e35',
          },
          headerTintColor: '#fff',
          headerTitleStyle: {
            fontWeight: 'bold',
          },
        header: null,
      }
    },
  },
  
});

const WeatherTabNavigator = createBottomTabNavigator({
  Today: {
    screen: TodayStack,
    
    navigationOptions: {
      tabBarLabel: 'Today',
      tabBarIcon: ({ tintColor }) => (
        <Icon 
        name='weather-rainy' 
          color= { tintColor } 
          size={24} 
        />
      )
    },
  },
  Forecast: {
    screen: ForecastStack,
    navigationOptions: {
      tabBarLabel: 'Forecast',
      tabBarIcon: ({ tintColor }) => (
        <Icon 
          name='feather' 
          color= { tintColor } 
          size={24} 
        />
      )
    },
  },
  Search: {
    screen: SearchStack,
    navigationOptions: {
      tabBarLabel: 'Search',
      tabBarIcon: ({ tintColor }) => (
        <Icon name='database-search' color= { tintColor } size={24} />
      )
    },
  },
},

  {
    navigationOptions: ({ navigation }) => {
      const { routeName } = navigation.state.routes[navigation.state.index];
      return {
        header: null,
        headerTitle: routeName,
        tabBarVisible: true,
      };
    },
    tabBarOptions: {
      activeTintColor: '#ffff00',
      inactiveTintColor: '#ccc',
      style:{
        backgroundColor: '#000',
        // backgroundColor: '#e91e63',
        borderTopWidth: 0.5,
        borderTopColor: 'white',
      },
      indicatorStyle:{
        height: 0,
      },
      showIcon: true,
    },
  }
);

const WeatherStackNavigator = createStackNavigator(
  {
    WeatherTabNavigator: {
      screen: WeatherTabNavigator
    },
    defaultNavigationOptions: ({ navigation }) => {
      return {
        header: null,
      };
    }
  }
);

const HomeStack = createStackNavigator({
  Home:{
    screen: WelcomeScreen,
    navigationOptions: ({ navigation }) => {
      return {
        header: null,
        
      }
    },
  },
  Staff: {
    screen: StaffScreen,
    navigationOptions: ({ navigation }) => {
      return {
        headerTitle: 'Teacher & Staff Portal',
        headerStyle: {
          backgroundColor: '#c62020',
        },
        headerTintColor: '#fff',
        headerTitleStyle: {
          fontWeight: 'bold',
        },
      }
    },
  },
  Weather: {
    screen: WeatherStackNavigator,
    navigationOptions: ({ navigation }) => {
      return {
        headerTitle: 'Current Location Info',
          headerStyle: {
            backgroundColor: '#e91e63',
          },
          headerTintColor: '#fff',
          headerTitleStyle: {
            fontWeight: 'bold',
          },
        // header: null,
      }
    },
  },
  Videos: {
    screen: Videos,
    navigationOptions: ({ navigation }) => {
      return {
        headerTitle: 'ITF Youtube Channel',
          headerStyle: {
            backgroundColor: '#e91e63',
          },
          headerTintColor: '#fff',
          headerTitleStyle: {
            fontWeight: 'bold',
          },
        // header: null,
      }
    },
  },
  Photos: {
    screen: Photos,
    navigationOptions: ({ navigation }) => {
      return {
        headerTitle: 'Morgan State Photos',
          headerStyle: {
            backgroundColor: '#e91e63',
          },
          headerTintColor: '#fff',
          headerTitleStyle: {
            fontWeight: 'bold',
          },
        // header: null,
      }
    },
  },
  ScrapeWeb: {
    screen: ScrapeData,
    navigationOptions: ({ navigation }) => {
      return {
        headerTitle: 'Amazon Graphic Cards',
          headerStyle: {
            backgroundColor: '#e91e63',
          },
          headerTintColor: '#fff',
          headerTitleStyle: {
            fontWeight: 'bold',
          },
        // header: null,
      }
    },
  },
  Student: {
    screen: StudentScreen,
    navigationOptions: ({ navigation }) => {
      return {
        headerTitle: 'Student Portal',
        headerStyle: {
          backgroundColor: '#006600',
        },
        headerTintColor: '#fff',
        headerTitleStyle: {
          fontWeight: 'bold',
        },
        // header: null,
        // headerLeft: (
        //   <Icon style={{ paddingLeft: 10 }} onPress={() => navigation.openDrawer()} name="menu" size={30} />
        // )
      }
    },
  },
  Guest: {
    screen: GuestScreen,
    navigationOptions: ({ navigation }) => {
      return {
        headerTitle: 'Welcome Guest',
        headerStyle: {
          backgroundColor: '#0000ff',
          // backgroundColor: '#f47937',
        },
        headerTintColor: '#fff',
        headerTitleStyle: {
          fontWeight: 'bold',
        },
        // header: null,
        // headerLeft: (
        //   <Icon style={{ paddingLeft: 10 }} onPress={() => navigation.openDrawer()} name="menu" size={30} />
        // )
      }
    },
  },
  News: {
    screen: News,
    navigationOptions: ({ navigation }) => {
      return {
        headerTitle: 'Morgan in the News',
        headerStyle: {
          backgroundColor: '#610dc7',
        },
        headerTintColor: '#fff',
        headerTitleStyle: {
          fontWeight: 'bold',
        },
        // header: null,
        // headerLeft: (
        //   <Icon style={{ paddingLeft: 10 }} onPress={() => navigation.openDrawer()} name="menu" size={30} />
        // )
      }
    },
  },
  Alumni: {
    screen: AlumniScreen,
    navigationOptions: ({ navigation }) => {
      return {
        headerTitle: 'Welcome Alum',
        headerStyle: {
          backgroundColor: '#e91e63',
        },
        headerTintColor: '#fff',
        headerTitleStyle: {
          fontWeight: 'bold',
        },
        // header: null,
        // headerLeft: (
        //   <Icon style={{ paddingLeft: 10 }} onPress={() => navigation.openDrawer()} name="menu" size={30} />
        // )
      }
    },
  }
},
{
  defaultNavigationOptions: {
    gesturesEnabled: false,
    // flex: 6,
  }
});

const ProfileStack = createStackNavigator({
  Profile: {
    screen: ProfileScreen,
    navigationOptions: ({ navigation }) => {
      return {
        headerTitle: 'Profile',
        headerStyle: {
          backgroundColor: '#e5e',
        },
        headerTintColor: '#fff',
        headerTitleStyle: {
          fontWeight: 'bold',
        },
          headerLeft: (
            <Icon style={{ paddingLeft: 10 }} onPress={() => navigation.openDrawer()} color= 'white' name="menu" size={30} />
          )
        
      }
    }
  }
});

const SettingsStack = createStackNavigator({
  Settings: {
    screen: SettingScreen,
    navigationOptions: ({ navigation }) => {
      return {
        headerTitle: 'Settings',
        headerStyle: {
          backgroundColor: '#e84e35',
        },
        headerTintColor: '#fff',
        headerTitleStyle: {
          fontWeight: 'bold',
        },
        headerLeft: (
          <Icon style={{ paddingLeft: 10 }} onPress={() => navigation.openDrawer()} color='white' name="menu" size={30} />
        )
      };
    }
  }
});

const DashboardTabNavigator = createBottomTabNavigator({
  Home: { 
    screen: HomeStack, 
    
    navigationOptions: {
      tabBarLabel: 'Home',
      tabBarIcon: ({ tintColor }) => (
        <Icon name='home' color= { tintColor } size={24} />
      )
    },
    
  },
  Profile: { 
    screen: ProfileStack, 
    navigationOptions: {
      tabBarLabel: 'Profile',
      tabBarIcon: ({ tintColor }) => (
          <Icon name='account' color= { tintColor } size={24} />
      )
    }
  },
  Settings: { 
    screen: SettingsStack, 
    navigationOptions: {
      tabBarLabel: 'Settings',
      tabBarIcon: ({ tintColor }) => (
          <Icon name='settings' color= { tintColor } size={24} />
      )
    }
  },
  },
// Below configures the tab Bar and nav options
// to avoid conflict between navigation options
// and tabBar options, ensure you position the tabBar options
//  after the navigationOptions in the same object
// as shown below. Otherwise weird things will happen
{
  navigationOptions: ({ navigation }) => {
    const { routeName } = navigation.state.routes[navigation.state.index];
    return {
      header: null,
      headerTitle: routeName,
      tabBarVisible: true,
    };
  },
  tabBarOptions: {
    activeTintColor: '#ffff00',
    inactiveTintColor: '#ccc',
    style:{
      // backgroundColor: '#610dc7',
      backgroundColor: '#000',
      borderTopWidth: 0.5,
      borderTopColor: 'white',
    },
    indicatorStyle:{
      height: 0,
    },
    showIcon: true,
  },
}
);

const DashboardStackNavigator = createStackNavigator(
  {
    DashboardTabNavigator: {
      screen: DashboardTabNavigator
    },
    defaultNavigationOptions: ({ navigation }) => {
      return {
        header: null,
      };
    }
  });


const AppDrawerNavigator = createDrawerNavigator(
{
  MyDashboard: {
    screen: DashboardStackNavigator,
  },
  Home: { screen: HomeStack},
  Weather: { screen: WeatherStackNavigator},
},
{
  // drawerPosition: Platform.OS == 'ios'? 'right': 'left',
  contentOptions: {
    activeTintColor: '#e91e63', 
  },
  contentComponent: DrawerMenu,
});

const AppSwitchNavigator = createSwitchNavigator({
  MyDashboard: { screen: AppDrawerNavigator },
  
});

const AppContainer = createAppContainer(AppSwitchNavigator);
  
const styles = StyleSheet.create({
    container: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center'
    },
    headerIconStyle: {
      // color: '#fff',
      //   flexDirection: 'row',
      fontSize: 60,
      alignItems: 'center',
      justifyContent: 'center'
  },
  headerIconSocial: {

    color: '#fff',
    flexDirection: 'row',
    flex: 2,
    fontSize: 20,
    alignItems: 'center',
    justifyContent: 'space-between'

  },
});